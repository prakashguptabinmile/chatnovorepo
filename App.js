import React, {useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import StackAuth from './src/navigations/StackAuth';
import ScreenSplash from './src/screens/BeforeAuthentication/ScreenSplash';

export default function App() {
  const [isLoad, setIsLoad] = useState(true);

  setTimeout(() => {
    setIsLoad(false);
  }, 3000);
  return isLoad ? (
    <ScreenSplash />
  ) : (
    <NavigationContainer>
      <StackAuth />
    </NavigationContainer>
  );
}
