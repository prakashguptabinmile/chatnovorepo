import 'react-native-gesture-handler';
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import ScreenWelcome from '../screens/BeforeAuthentication/ScreenWelcome';
import ScreenLogin from '../screens/BeforeAuthentication/ScreenLogin';
import ScreenCheckAuth from '../screens/BeforeAuthentication/ScreenCheckAuth';
import StackApp from './StackApp';

const Stack = createNativeStackNavigator();
export default function StackAuth() {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false, animationEnabled: false}}>
      <Stack.Screen name="ScreenWelcome" component={ScreenWelcome} />
      <Stack.Screen name="ScreenLogin" component={ScreenLogin} />
      <Stack.Screen name="ScreenCheckAuth" component={ScreenCheckAuth} />
      <Stack.Screen name="StackApp" component={StackApp} />
    </Stack.Navigator>
  );
}
