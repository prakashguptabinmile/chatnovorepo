import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import ScreenHeader from '../components/ScreenHeader';
import Colors from '../assets/Styles/colorStyles';
import fontWeight from '../assets/Styles/fontWeightStyles';
import ScreenChats from '../screens/AfterAuthentication/screenchats/ScreenChats';
import ScreenCalls from '../screens/AfterAuthentication/screenCalls/ScreenCalls';
import ScreenStatus from '../screens/AfterAuthentication/ScreenStatus/ScreenStatus';

const Tab = createMaterialTopTabNavigator();

export default function StackApp() {
  return (
    <>
      <ScreenHeader />
      <Tab.Navigator
        screenOptions={{
          tabBarActiveTintColor: Colors.WhiteColor,
          tabBarLabelStyle: {fontSize: 16, fontWeight: fontWeight.Bold},
          tabBarStyle: {backgroundColor: Colors.BlueColor},
          tabBarIndicatorStyle: {
            borderBottomColor: Colors.WhiteColor,
            borderBottomWidth: 3,
            width: '30%',
            left: '2%',
            right: '2%',
          },
        }}>
        <Tab.Screen name="CHATS" component={ScreenChats} />
        <Tab.Screen name="STATUS" component={ScreenStatus} />
        <Tab.Screen name="CALLS" component={ScreenCalls} />
      </Tab.Navigator>
    </>
  );
}
