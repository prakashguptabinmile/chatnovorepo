import {StyleSheet} from 'react-native';

const Colors = StyleSheet.create({
  WhiteColor: '#fff',
  BlackColor: '#000000',
  BlueColor: '#0BA8E6',
  GreyColor: '#505050',
});

export default Colors;

