import {StyleSheet} from 'react-native';

const fontWeight = StyleSheet.create({
  Bold: 'bold',
  semiBold: '600',
});

export default fontWeight;
