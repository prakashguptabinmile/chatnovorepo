import {View, Text, StyleSheet, Image} from 'react-native';
import React from 'react';
import MyImg from '../assets/images/prakashPhoto.jpg';
import Colors from '../assets/Styles/colorStyles';
import fontWeight from '../assets/Styles/fontWeightStyles';
// import localization from '../assets/localization/localization';

export default function ScreenChatProfile({Name, Time}) {
  return (
    <View style={styles.profileContainer}>
      <Image source={MyImg} style={styles.profileImage} />
      <View style={styles.profileTextContent}>
        {/* <Text style={styles.profileTitle}>{localization.profileName}</Text>
        <Text style={styles.profileTime}>{localization.profileTime}</Text> */}
        <View>
          <Text style={styles.profileTitle}>{Name}</Text>
        </View>
        <View>
          <Text style={styles.profileTime}>{Time}</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  profileContainer: {
    height: 90,
    alignItems: 'center',
    backgroundColor: Colors.WhiteColor,
    borderRadius: 15,
    shadowColor: Colors.BlueColor,
    shadowOpacity: 1,
    shadowRadius: 8,
    elevation: 8,
    flexDirection: 'row',
    paddingLeft: 16,
    paddingRight: 14,
    marginTop: 6,
    marginHorizontal: 10,
  },
  profileImage: {
    height: 50,
    width: 50,
    borderRadius: 50,
  },
  profileTextContent: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 20,
    marginRight: 10,
  },
  profileTitle: {
    color: Colors.BlackColor,
    fontWeight: fontWeight.semiBold,
    fontSize: 18,
  },
  profileTime: {
    fontSize: 14,
    color: Colors.GreyColor,
    fontWeight: fontWeight.semiBold,
  },
});
