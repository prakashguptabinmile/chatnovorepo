import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import Colors from '../assets/Styles/colorStyles';
import fontWeight from '../assets/Styles/fontWeightStyles';
import localization from '../assets/localization/localization';

export default function Button({onPress, BtnTitle, width}) {
  return (
    <>
      <TouchableOpacity
        style={[styles.agreeBtn, (width = {width})]}
        onPress={onPress}>
        <Text style={styles.btnText}>{BtnTitle} </Text>
      </TouchableOpacity>
    </>
  );
}

const styles = StyleSheet.create({
  agreeBtn: {
    backgroundColor: Colors.BlueColor,
    // paddingHorizontal: 40,
    paddingVertical: 10,
    borderRadius: 6,
    marginTop: 25,
    marginBottom: 50,
    alignSelf: 'center',
  },
  btnText: {
    fontSize: 16,
    textAlign: 'center',
    color: Colors.BlackColor,
    fontWeight: fontWeight.semiBold,
  },
});
