import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import React from 'react';
import localization from '../assets/localization/localization';
import DotIcon from 'react-native-vector-icons/Entypo';
import SearchIcon from 'react-native-vector-icons/Ionicons';
import Colors from '../assets/Styles/colorStyles';
import fontWeight from '../assets/Styles/fontWeightStyles';

export default function ScreenHeader() {
  return (
    <View style={styles.headerContainer}>
      <Text style={styles.headerTitle}>{localization.headerTitle}</Text>
      <View style={styles.iconContainer}>
        <TouchableOpacity>
          <SearchIcon name="search" style={styles.headerIcons} />
        </TouchableOpacity>
        <TouchableOpacity>
          <DotIcon name="dots-three-vertical" style={styles.headerIcons} />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: Colors.BlueColor,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingTop: 30,
    paddingBottom: 15,
  },
  headerTitle: {
    color: Colors.WhiteColor,
    fontWeight: fontWeight.semiBold,
    fontSize: 22,
  },
  iconContainer: {
    flexDirection: 'row',
  },
  headerIcons: {
    marginLeft: 15,
    color: Colors.WhiteColor,
    fontSize: 25,
  },
});
