import {
  View,
  Text,
  TextInput,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import React from 'react';
import localization from '../../assets/localization/localization';
import styles from './stylesLogin';
import Button from '../../components/Button';

export default function ScreenLogin({navigation}) {
  return (
    <>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
          <View>
            <Text style={styles.loginTitle}>{localization.enterMobile}</Text>
            <Text style={styles.loginVerifyText}>
              {localization.needToVerifyText}
            </Text>
            <Text style={styles.loginVerifyNum}>{localization.whatMyNum}</Text>
            <TextInput
              placeholder="(+91)  phone number"
              placeholderTextColor="black"
              keyboardType="numeric"
              style={styles.loginInputBox}
            />
            <Button
              BtnTitle={localization.nextBtn}
              onPress={() => navigation.navigate('StackApp')}
              width={150}
            />
          </View>
        </KeyboardAvoidingView>
      </TouchableWithoutFeedback>
    </>
  );
}
