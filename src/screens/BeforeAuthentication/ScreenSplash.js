import {View, Text, ImageBackground, StyleSheet} from 'react-native';
import React from 'react';
import SplashImg from '../../assets/images/chatlogo1.png';
import Colors from '../../assets/Styles/colorStyles';
import fontWeight from '../../assets/Styles/fontWeightStyles';

export default function ScreenSplash() {
  return (
    <View style={styles.splashContainer}>
      <ImageBackground
        source={SplashImg}
        resizeMode="cover"
        style={styles.splashBgImg}></ImageBackground>
    </View>
  );
}
const styles = StyleSheet.create({
  splashContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.BlackColor,
  },
  splashBgImg: {
    alignSelf: 'center',
    width: 300,
    height: 300,
  },
  splashText: {
    color: Colors.BlackColor,
    fontSize: 32,
    fontWeight: fontWeight.Bold,
    textAlign: 'center',
  },
});
