import {StyleSheet} from 'react-native';
import Colors from '../../assets/Styles/colorStyles';
import fontWeight from '../../assets/Styles/fontWeightStyles';

const styles = StyleSheet.create({
  loginTitle: {
    color: Colors.BlueColor,
    fontSize: 22,
    textAlign: 'center',
    fontWeight: '600',
    marginTop: 30,
  },
  loginVerifyText: {
    marginTop: 40,
    textAlign: 'center',
    color: Colors.BlackColor,
    fontSize: 16,
  },

  loginVerifyNum: {
    color: Colors.BlueColor,
    textAlign: 'center',
    fontSize: 16,
    marginTop: 10,
  },
  loginInputBox: {
    borderTopWidth: 1,
    borderTopColor: Colors.BlueColor,
    borderBottomColor: Colors.BlueColor,
    borderBottomWidth: 1,
    marginHorizontal: 20,
    paddingHorizontal: 20,
    marginTop: 50,
    marginBottom: 100,
    color: Colors.BlackColor,
  },
});

export default styles;
