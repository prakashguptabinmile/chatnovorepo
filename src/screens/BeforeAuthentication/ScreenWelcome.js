import {View, Text, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import styles from './stylesWelcome';
import ChatLogo from '../../assets/images/chatlogo1.png';
import localization from '../../assets/localization/localization';
import Button from '../../components/Button';

export default function ScreenWelcome({navigation}) {
  return (
    <View style={styles.container}>
      <Text style={styles.welcomeText}>{localization.welcomeTitle}</Text>
      <Image source={ChatLogo} style={styles.logoView} />
      <Text style={styles.policyText}>
        {localization.readOur}
        <Text style={styles.blueText}>{localization.privacyPolicy}</Text>{' '}
        {localization.agreeAndContinue}
        <Text style={styles.blueText}>{localization.termAndService}</Text>
      </Text>
      <Button
        BtnTitle={localization.agreeBtn}
        onPress={() => navigation.navigate('ScreenLogin')}
        width={300}
      />
      <Text style={styles.policyText}>{localization.from}</Text>
      <Text style={styles.binmileText}>{localization.company}</Text>
    </View>
  );
}
