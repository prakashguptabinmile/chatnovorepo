import {StyleSheet} from 'react-native';
import Colors from '../../assets/Styles/colorStyles';
import fontWeight from '../../assets/Styles/fontWeightStyles';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  welcomeText: {
    color: Colors.BlueColor,
    fontSize: 28,
    textAlign: 'center',
    marginTop: 40,
    fontWeight: fontWeight.Bold,
  },
  logoView: {
    width: 320,
    height: 320,
    alignSelf: 'center',
    marginVertical: 50,
  },
  policyText: {
    color: Colors.BlackColor,
    marginHorizontal: 15,
    fontSize: 13,
    textAlign: 'center',
  },
  blueText: {
    color: Colors.BlueColor,
  },
  binmileText: {
    textAlign: 'center',
    letterSpacing: 2,
    marginVertical: 5,
    color: Colors.BlackColor,
    fontWeight: fontWeight.semiBold,
  },
});

export default styles;
