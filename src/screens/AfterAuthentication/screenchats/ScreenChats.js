import {View, Text, FlatList} from 'react-native';
import React from 'react';
import ScreenChatProfile from '../../../components/ScreenChatProfile';

export default function ScreenChats() {
  const DATA = [
    {
      id: '0',
      name: 'Prakash Gupta',
      time: '2:30',
    },
    {
      id: '1',
      name: 'Suraj Shrestha',
      time: '2:30',
    },
    {
      id: '2',
      name: 'Ajay Kumar',
      time: '2:30',
    },
    {
      id: '3',
      name: 'Ajay Kumar',
      time: '2:30',
    },
    {
      id: '4',
      name: 'Ajay Kumar',
      time: '2:30',
    },
    {
      id: '5',
      name: 'Ajay Kumar',
      time: '2:30',
    },
    {
      id: '12',
      name: 'Ajay Kumar',
      time: '2:30',
    },
    {
      id: '6',
      name: 'Ajay Kumar',
      time: '2:30',
    },
    {
      id: '7',
      name: 'Ajay Kumar',
      time: '2:30',
    },
    {
      id: '8',
      name: 'Ajay Kumar',
      time: '2:30',
    },
    {
      id: '9',
      name: 'Ajay Kumar',
      time: '2:30',
    },
    {
      id: '10',
      name: 'Ajay Kumar',
      time: '2:30',
    },
  ];
  return (
    <View>
      <FlatList
        data={DATA}
        renderItem={({item}) => (
          <ScreenChatProfile Name={item.name} Time={item.time} />
        )}
        keyExtractor={item => item.id}
      />
    </View>
  );
}
